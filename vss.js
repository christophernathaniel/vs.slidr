let vss = function(sid, xsid) {
    let d=document;i=0;count=[];slides=[];slide=[];ele=[];loop=[];pl =[];prevdirect=[];drct =[]; // Define Variables
    function qsa(x, ele){return x.querySelectorAll(ele);} 
    id=qsa(d,sid); 
    
    for (var vss of id) { 
        i++; count[i] = 0; ele[i] = vss; 

        ele[i].classList.add('vss-s');
        slides[i] = qsa(ele[i], xsid); 
        
        slide[i] = [];
        for (var slidei of slides[i]) { 
            ++count[i]; 
            x = count[i]; 
            slidei.classList.add('vss-i');
            slidei.dataset.number = x;
            slide[i][x] = slidei;
        }
    
        (function run(i) {  setTimeout(function(){ run(i); trig(i,"fw"); }, 8000); })(i);

        ele[i].parentNode.getElementsByClassName('left')[0].addEventListener('click', trig.bind(null, i, "rw"), false); 
        ele[i].parentNode.getElementsByClassName('right')[0].addEventListener('click', trig.bind(null, i, "fw"), false); 

        function trig(i, direct) {
            drct[i] = direct;
            if(drct[i] == "fw") {
                if(!loop[i]) { loop[i] = count[i]; pl[i] = 1; }
                    setTimeout(() => {
                        ele[i].style.position = "relative";
                        ele[i].style.transitionDuration = '0ms';
                        ele[i].style.left =  "0px";
                        slide[i][pl[i]].after(slide[i][loop[i]]); 
                    }, 300);
                    ele[i].style.position = "relative";
                    ele[i].style.transitionDuration = '200ms';
                    ele[i].style.left =  -slide[i][1].offsetWidth;
                if(prevdirect[i] == "rw") {
                    if(loop[i] == 1)    { loop[i] = count[i]; pl[i] = loop[i] - 1 
                    } else              { loop[i] = loop[i] - 1; pl[i] = loop[i] - 1 }
                } else {
                    ++loop[i]; pl[i] = loop[i] - 1 }
                if(loop[i] > count[i])  { loop[i] = 1; pl[i] = count[i] }
                if(pl[i] > count[i])    { pl[i] = 1; loop[i] = 2 }
                if(pl[i] < 1)           { pl[i] = count[i] }
            }  
           
            if(drct[i] == "rw") {
                if(!loop[i]) { loop[i] = 2; pl[i] = 1; }
                    setTimeout(() => {
                        ele[i].style.position = "relative";
                        ele[i].style.transitionDuration = '200ms';
                        ele[i].style.left =  0;
                    }, 300);
                    ele[i].style.left =  -slide[i][1].offsetWidth;
                    ele[i].style.transitionDuration = '0ms';
                    ele[i].style.position = "relative";
                    if(prevdirect[i] == "fw") {
                        if(loop[i] == count[i])   { loop[i] = 1;
                        } else                    { loop[i] = loop[i] + 1 }
                        pl[i] = loop[i] - 1;
                    } else { --loop[i];  pl[i] = loop[i] - 1; }
                if(loop[i] < 1) { loop[i] = count[i]; pl[i] = count[i] - 1 } 
                if(pl[i] < 1) { pl[i] = count[i]; loop[i] = 1; }
                slide[i][loop[i]].before(slide[i][pl[i]]);
            }
            prevdirect[i] = drct[i];
        }
    }
}

let initvss = new vss('slider', 'slide');
